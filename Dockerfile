FROM nginx as base
WORKDIR /var/www/app
COPY ./deploy/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./deploy/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY ["./deploy/nginx/security.conf","./deploy/nginx/general.conf", "./deploy/nginx/proxy.conf", "/etc/nginx/nginxconfig.io/"]

FROM node:16 as build
WORKDIR /var/www/app
COPY ["package.json", "yarn.lock", "/var/www/app/"]
RUN yarn
COPY . /var/www/app/
RUN  npm run build

FROM base
COPY --from=build /var/www/app/dist/spa /var/www/app/
