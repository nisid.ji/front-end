import member from "./member.vue";

export default {
  functional: true,
  name: "Report-Member-Function",
  render(h, ctx) {
    const store = ctx.parent.$store;
    const component = require("./member_v3.vue");
    return h(component.default, ctx.data, ctx.children);
    // if (store.state.auth.role < 2 && store.state.auth.role > -1) {
    //   const component = require('./member.vue');
    //   return h(component.default, ctx.data, ctx.children)
    // } else {
    //   const component = require('./member.vue');
    //   return h(component.default, ctx.data, ctx.children)
    // }
  },
  beforeRouteLeave(to, from, next) {
    if (this.beforeRouteLeave) {
      this.beforeRouteLeave(to, from, next);
    }
  }
};
