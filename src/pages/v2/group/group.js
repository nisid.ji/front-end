import AdminGroup from './admin/group'
import Group from './group.vue'

export default {
  functional: true,
  name: 'Group',
  render(h, ctx) {
    const store = ctx.parent.$store;
    if (store.state.auth.role < 2 && store.state.auth.role > -1) {
      return h(AdminGroup, ctx.data, ctx.children)
    }
    return h(Group, ctx.data, ctx.children)
  }
};
