import {api} from "boot/axios";

export function load({rootState, commit}, fileter = {}) {
  return new Promise(async (resolve, reject) => {
    try {
      if (!fileter.role_level) {
        fileter.role_level = 2;
      }
      let response = await api.get('/api/v1/member', {
        params: {
          ...fileter
        }
      });
      commit('listMutation', response.data)
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}

export function changePassword({commit}, params = {id: undefined, new_password: undefined}) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.post(`/api/v1/member/${params.id}/reset-password`, {
        new_password: params.new_password
      });
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}
