import {api} from "boot/axios";

export function auth(context, {username, password}) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.post('/api/v1/authenticate/login', {username, password});
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}

export function logout(context) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.post('/api/v1/authenticate/logout', data);
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}

export function changePassword(context, data) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.post('/api/v1/authenticate/reset-password', data);
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}
