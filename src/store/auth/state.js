import {LocalStorage} from "quasar";

export default function () {
  return {
    //
    credit: LocalStorage.has("credit") ?  LocalStorage.getItem("credit") : 0 ,
    role: LocalStorage.has("role") ?  LocalStorage.getItem("role") : -1 ,
    username:  LocalStorage.has("username") ?LocalStorage.getItem("username") : "unknown",
    user_permissions: LocalStorage.has("user_permissions") ? LocalStorage.getItem("user_permissions"): [],
    key: LocalStorage.has("key") ? LocalStorage.getItem("key") : "",
  }
}
