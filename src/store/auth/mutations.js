import { LocalStorage } from "quasar";

export function set_user(
  state,
  {
    credit = 0,
    key = "",
    role = -1,
    user_permissions = {},
    username = "unknown"
  }
) {
  LocalStorage.clear();
  state.credit = credit;
  state.key = key;
  state.role = role;
  state.username = username;
  state.user_permissions = user_permissions;
  LocalStorage.set("credit", state.credit);
  LocalStorage.set("key", state.key);
  LocalStorage.set("role", state.role);
  LocalStorage.set("username", state.username);
  LocalStorage.set("user_permissions", state.user_permissions);
}

export function set_logout(state) {
  LocalStorage.clear();
  state.credit = 0;
  state.key = "";
  state.role = -1;
  state.username = "";
  state.user_permissions = [];
}

export function set_credit(state, payload) {
  state.credit += payload;
}
