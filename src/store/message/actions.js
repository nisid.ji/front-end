import {api} from "boot/axios";

export function load(context, data) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.get('/api/v1/message', {
        params: {
          status: 1
        }
      });
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}
