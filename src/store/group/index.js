import v2 from './v2'

export default {
  namespaced: true,
  modules : {
    v2: v2
  }
}

