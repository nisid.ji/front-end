import state from './state'
import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'
import v2 from "./v2"
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
  modules : {
    v2: v2
  }
}
