import {LocalStorage} from "quasar";

/*
export function someGetter (state) {
}
*/
export function permission(state, permission_name) {
  const permissionList = LocalStorage.getItem("user_permissions") || {};
  const permissions = permissionList[permission_name];
  if (permissions) {
    return (type) => {
      return permissions.includes(`${type}_${permission_name}`);
    }
  }
  return () => false;
}
