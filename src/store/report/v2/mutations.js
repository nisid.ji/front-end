/*
export function someMutation (state) {
}
*/

export function setDateTime(state, payload) {
  state.dateTime = payload;
}

export function setGameType(state, payload) {
  state.game_type = payload;
}

export function setProvider(state, payload) {
  state.provider_code = payload;
}
