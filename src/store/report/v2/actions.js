import { api } from "boot/axios";

export async function getAllReport(
  { commit },
  params = {
    page: undefined,
    limit: undefined,
    start: undefined,
    end: undefined,
    providerCode: undefined,
    typeCode: undefined
  }
) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.get("/api/progress/", {
        params: {
          ...params
        }
      });
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}

export async function getOrganization(
  { commit },
  params = {
    page: undefined,
    limit: undefined,
    start: undefined,
    end: undefined,
    providerCode: undefined,
    typeCode: undefined,
    search: undefined
  }
) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.get("/api/progress/organization/", {
        params: {
          ...params
        }
      });
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}

export async function getOrganizationById(
  { commit },
  params = {
    page: undefined,
    limit: undefined,
    start: undefined,
    end: undefined,
    providerCode: undefined,
    typeCode: undefined,
    id: undefined,
    search: undefined
  }
) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.get(`/api/progress/organization/${params.id}`, {
        params: {
          page: params.page,
          limit: params.limit,
          start: params.start,
          end: params.end,
          providerCode: params.providerCode,
          typeCode: params.typeCode,
          search: params.search
        }
      });
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}

export async function getTransactionById(
  { commit },
  params = {
    page: undefined,
    limit: undefined,
    start: undefined,
    end: undefined,
    providerCode: undefined,
    typeCode: undefined,
    id: undefined
  }
) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.get(
        `/api/progress/organization/${params.id}/transaction`,
        {
          params: {
            page: params.page,
            limit: params.limit,
            start: params.start,
            end: params.end,
            providerCode: params.providerCode,
            typeCode: params.typeCode
          }
        }
      );
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}

export async function load_summary(
  { commit },
  params = {
    page: 1,
    datetime_start: undefined,
    datetime_end: undefined,
    provider_code: undefined,
    game_type: undefined,
    parent_id: undefined,
    username: undefined
  }
) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.get("/api/report/v2/dashboard/summary", {
        params: {
          ...params
        }
      });
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}

export async function load_dashboardV1(
  { commit },
  params = {
    page: 1,
    datetime_start: undefined,
    datetime_end: undefined,
    provider_code: undefined,
    game_type: undefined,
    parent_id: undefined,
    username: undefined
  }
) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.get("/api/progress/account", {
        params: {
          // ...params
        },
        mode: "CORS",
        headers: {
          Accept: "application/json",
          Cookie: null
        },
        withCredentials: false
      });
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}

export async function load_member_transaction(
  { commit },
  params = {
    datetime_start: undefined,
    datetime_end: undefined,
    provider_code: undefined,
    game_type: undefined,
    parent_id: undefined,
    transaction: undefined
  }
) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await api.get("/api/report/v2/dashboard", {
        params: {
          ...params
        }
      });
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
}

// export async function load_member_transaction(
//   { commit },
//   params = {
//     datetime_start: undefined,
//     datetime_end: undefined,
//     provider_code: undefined,
//     game_type: undefined,
//     parent_id: undefined,
//     transaction: undefined
//   }
// ) {
//   return new Promise(async (resolve, reject) => {
//     try {
//       let response = await api.get("/api/report/v2/dashboard/transaction", {
//         params: {
//           ...params
//         }
//       });
//       resolve(response);
//     } catch (error) {
//       reject(error);
//     }
//   });
// }
