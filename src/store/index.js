import Vue from 'vue'
import Vuex from 'vuex'

// we first import the module
import group from './group'
import auth from './auth'
import permission from './permission'
import message from "./message";
import provider from "./provider";
import type from "./type";
import member from "./member";
import report from "./report";

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // then we reference it
      group,
      auth,
      permission,
      message,
      provider,
      type,
      member,
      report
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  /*
    if we want some HMR magic for it, we handle
    the hot update like below. Notice we guard this
    code with "process.env.DEV" -- so this doesn't
    get into our production build (and it shouldn't).
  */

  // if (process.env.DEV && module.hot) {
  //   module.hot.accept(['./v2'], () => {
  //     const newShowcase = require('./v2').default
  //     Store.hotUpdate({ modules: { showcase: newShowcase } })
  //   })
  // }

  return Store
}
