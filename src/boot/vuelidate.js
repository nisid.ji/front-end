import Vuelidate from 'vuelidate'

export default async ({ app, router, Vue } ) => {
  Vue.use(Vuelidate);
}
