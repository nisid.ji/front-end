import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

export default async ({ app, router, Vue }) => {
  Vue.use(BootstrapVue, IconsPlugin)  
}
