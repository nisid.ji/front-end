const level = role => {
  let roleName;
  switch (role) {
    case 0:
      roleName = "Admin";
      break;
    case 1:
      roleName = "Staff";
      break;
    case 2:
      roleName = "คอมปะนี";
      break;
    case 3:
      roleName = "ผู้ถือหุ่น";
      break;
    case 4:
      roleName = "ซีเนียร์";
      break;
    case 5:
      roleName = "เอเย่น";
      break;
    case 6:
      roleName = "สมาชิก";
      break;

    default:
      roleName = "unknow level";
      break;
  }
  return roleName;
};

export default async ({ Vue }) => {
  // something to do

  Vue.prototype.$convert = {
    level
  };
};
