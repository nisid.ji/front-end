import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ( { app, router, Vue } ) => {
  Vue.use(ElementUI, { locale })
}
