// import something here
import dayjs from "dayjs";
import 'dayjs/locale/th'
import utc from  'dayjs/plugin/utc'
dayjs.locale('th') // use Spanish locale globally
dayjs.extend(utc)

// "async" is optional;
window.dayjs = dayjs;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async (/* { app, router, Vue ... } */) => {
  // something to do
}
