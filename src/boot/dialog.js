import {Dialog} from 'quasar'

const errorException = (exception) => {
  let message = "";
  if (exception.response) {
    message = exception.response.data.message;
  } else {
    message = exception.message;
  }
  return error("Error", message, {
    color: "red"
  });
}

const error = (title = "", message = "", opt = {}) =>  Dialog.create({
    title: title,
    message: message,
  ...opt
  });

const alert = (title = "", message = "", opt = {}) => Dialog.create({
  title: title,
  message: message,
  color: "blue",
  ...opt
});



// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({Vue}) => {
  // something to do

  Vue.prototype.$dialog = {
    error,
    errorException,
    alert
  };

}
