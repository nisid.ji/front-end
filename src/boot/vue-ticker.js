import VueTicker from 'vue-ticker'

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({ app, router, Vue } ) => {
  Vue.use(VueTicker)
}
