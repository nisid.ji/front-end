// import Vue from "vue";
import axios from "axios";
import { LocalStorage, SessionStorage } from "quasar";

// we add it to Vue prototype
// so we can reference it in Vue files as this.$axios
// without the need to import axios or use vue-axios
// can also create an axios instance specifically for the backend API
const api = axios.create({
  baseURL: process.env.API_URL,
  timeout: 60 * 1000, // Timeout
  xsrfCookieName: "XSRF-TOKEN", // default
  xsrfHeaderName: "X-XSRF-TOKEN", // default
  withCredentials: true // Check cross-site Access-Control,
});

export default context => {
  const { Vue, router, store } = context;
  Vue.prototype.$axios = axios;
  api.interceptors.request.use(function(config) {
    const token = store.state.auth.key;
    if (token) {
      config.headers.Authorization = token;
    }
    return config;
  });
  api.interceptors.response.use(
    response => {
      return response;
    },
    error => {
      if (401 == error.response.status) {
        if (!error.response.config.url.startsWith("/api/progress/")) {
          store.dispatch("auth/logout").finally(() => {
            router.push({ path: "/login" });
            //window.location.href = "/logout";
          });
          return Promise.reject(error);
        }
        return Promise.reject({ message: "รายงานยังไม่พร้อมใช้งาน" });
      } else {
        return Promise.reject(error);
      }
    }
  );
  Vue.prototype.$api = api;
};

export { axios, api };
