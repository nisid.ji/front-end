import store from "../store";
import { LocalStorage, SessionStorage } from "quasar";

const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayoutV2.vue"),
    beforeEnter: (to, from, next) => {
      let permissions = LocalStorage.getItem("user_permissions");
      if (permissions) {
        permissions.report = ["report"];
      }
      SessionStorage.set("key", LocalStorage.getItem("key"));
      SessionStorage.set("permissions", permissions);
      SessionStorage.set("username", LocalStorage.getItem("username"));
      SessionStorage.set("credit", LocalStorage.getItem("credit"));
      SessionStorage.set("role", LocalStorage.getItem("role"));
      // if (from.matched.some(x => x.path == "/v2")) {
      //   next(`/v2${to.path}`);
      // } else {
      //   next();
      // }
      next();
    },
    children: [
      { path: "", redirect: "/v2" },
      {
        path: "/provider",
        name: "provider",
        component: () => import("pages/setting/Provider.vue")
      },
      {
        path: "/provideradd",
        name: "provideradd",
        component: () => import("pages/setting/ProviderAdd.vue")
      },
      {
        path: "/provideradd/:id",
        name: "provideredit",
        component: () => import("pages/setting/ProviderAdd.vue")
      },
      {
        path: "/type",
        name: "type",
        component: () => import("pages/setting/Type.vue")
      },
      {
        path: "/group",
        name: "group",
        component: () => import("pages/setting/Group.vue")
      },

      {
        path: "/message",
        name: "message",
        component: () => import("pages/setting/Messages.vue")
      },
      {
        path: "/role",
        name: "role",
        component: () => import("pages/setting/Role.vue")
      },
      {
        path: "/company",
        name: "company",
        component: () => import("pages/company/Company.vue")
      },
      {
        path: "/companydetail/:id",
        name: "companydetail",
        component: () => import("pages/company/CompanyDetail.vue")
      },
      {
        path: "/staff",
        name: "staff",
        component: () => import("pages/staff/Staff.vue")
      },
      {
        path: "/error403",
        name: "error403",
        component: () => import("pages/Error403.vue")
      },
      {
        path: "/owner",
        name: "owner",
        component: () => import("pages/downline/Owner.vue")
      },
      {
        path: "/share",
        name: "share",
        component: () => import("pages/downline/Share.vue")
      },
      {
        path: "/senior",
        name: "senior",
        component: () => import("pages/downline/Senior.vue")
      },
      {
        path: "/agent",
        name: "agent",
        component: () => import("pages/downline/Agent.vue")
      },
      {
        path: "/v1/agent",
        component: () => import("pages/downline/Agent.vue")
      },
      {
        path: "/v1/member",
        component: () => import("pages/downline/Member.vue")
      },
      { path: "/report", redirect: "/v2/report" }
      // {path: '/report', name: 'report', component: () => import('pages/reports/index.vue')},
      // {
      //   path: '/report/company/:company_id',
      //   name: 'report-company',
      //   component: () => import('pages/reports/company.vue')
      // },
    ]
  },
  {
    path: "/v2",
    component: () => import("layouts/MainLayoutV2.vue"),
    beforeEnter: (to, from, next) => {
      SessionStorage.set("key", LocalStorage.getItem("key"));
      SessionStorage.set(
        "permissions",
        LocalStorage.getItem("user_permissions")
      );
      SessionStorage.set("username", LocalStorage.getItem("username"));
      SessionStorage.set("credit", LocalStorage.getItem("credit"));
      SessionStorage.set("role", LocalStorage.getItem("role"));
      next();
    },
    children: [
      { path: "", redirect: "dashboard" },
      {
        path: "dashboard",
        component: () => import("pages/v2/index.vue"),
        meta: {
          breadcrumbs: [{ title: "Dashboard" }]
        }
      },
      {
        path: "landing-page",
        component: () => import("pages/v2/landing-page.vue")
      },
      {
        path: "member-page",
        component: () => import("pages/v2/member-page.vue")
      },
      {
        path: "group",
        component: () => import("pages/v2/group/index.vue"),
        meta: {
          breadcrumbs: [{ title: "Group", path: "/v2/group" }]
        }
      },
      {
        path: "group/add",
        component: () => import("pages/v2/group/group.js"),
        meta: {
          edit: false,
          breadcrumbs: [{ title: "Group", path: "/v2/group" }, { title: "Add" }]
        }
      },
      {
        path: "group/:id",
        component: () => import("pages/v2/group/group.js"),
        meta: {
          edit: true,
          breadcrumbs: [
            { title: "Group", path: "/v2/group" },
            { title: "Edit" }
          ]
        }
      },
      {
        path: "group/:id/provider",
        component: () => import("pages/v2/group/provider.vue"),
        meta: {
          edit: true,
          breadcrumbs: [
            { title: "Group", path: "/v2/group" },
            { title: "Provider" }
          ]
        }
      },
      {
        path: "group/:id/type",
        component: () => import("pages/v2/group/type.vue"),
        meta: {
          edit: true,
          breadcrumbs: [
            { title: "Group", path: "/v2/group" },
            { title: "Type" }
          ]
        }
      },
      {
        path: "report",
        name: "report",
        component: () => import("pages/v2/report/member_v3.vue"),
        // component: () => import('pages/v2/report/member.vue'),
        children: [
          {
            path: ":id",
            component: () => import("pages/v2/report/member_v3.vue"),
            meta: {
              breadcrumbs: [{ title: "Dashboard" }]
            }
          }
        ],
        meta: {
          breadcrumbs: [{ title: "Report" }]
        }
      },
      {
        path: "report/member",
        name: "report-member",
        component: () => import("pages/v2/report/member_v3.vue"),
        meta: {
          level: 6,
          breadcrumbs: [{ title: "Report Member", path: "/v2/report/member" }]
        }
      },
      {
        path: "reportProvider",
        name: "report-provider",
        component: () => import("pages/v2/reportProvider"),
        meta: {
          level: 6,
          breadcrumbs: [{ title: "Report Member", path: "/v2/report/provider" }]
        }
      },
      {
        path: "report/:id/member",
        alias: "report/:id",
        name: "report-child-member",
        component: () => import("pages/v2/report/index.js"),
        meta: {
          level: 6,
          breadcrumbs: [
            { title: "Report Member", path: "/v2/report/member" },
            { title: "Downline" }
          ]
        }
      },
      {
        path: "report/:id/:username",
        alias: "report/:id",
        name: "report-child",
        component: () => import("pages/v2/report/member.vue"),
        meta: {
          breadcrumbs: [
            { title: "Report", path: "/v2/report" },
            { title: "Downline" }
          ]
        }
      },

      {
        path: "report/member/:id/:username",
        alias: "report/:id",
        name: "report-member-with-member",
        component: () => import("pages/v2/report/member_member_v3.vue"),
        meta: {
          breadcrumbs: [
            { title: "Report Member", path: "/v2/report/member" },
            { title: "Member" }
          ]
        }
      },
      {
        path: "transaction",
        name: "transaction",
        component: () => import("pages/v2/reportTransection")
      }
    ]
  },
  {
    path: "/v2/login",
    component: () => import("pages/v2/login.vue")
  },
  {
    path: "/login",
    component: () => import("layouts/login"),
    beforeEnter: (to, from, next) => {
      next("/v2/login");
    },
    children: [
      {
        path: "/login",
        name: "login",
        component: () => import("pages/login/Login.vue")
      }
    ]
  },
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
