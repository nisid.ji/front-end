import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import {LocalStorage, SessionStorage} from 'quasar'

Vue.use(VueRouter)
/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/*{ store, ssrContext }*/) {
  const Router = new VueRouter({
    scrollBehavior: () => ({x: 0, y: 0}),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  // Router.beforeEach((to, from, next) => {
  //   let path = {
  //     staff: 'member',
  //     provideradd: 'game',
  //     companydetail: 'company',
  //     provideredit: 'game',
  //     owner: 'company',
  //     share: 'member',
  //     senior: 'member',
  //     agent: 'member'
  //   }
  //   let name = path[to.name] == undefined ? to.name : path[to.name]
  //   let setting = ['role', 'game', 'message', 'provider', 'provideredit', 'provideradd', 'gametype']
  //   try {
  //     if (SessionStorage.isEmpty() == true && (name != 'login' && name != 'Error404' && to.name != undefined)) {
  //       next('/login')
  //     } else {
  //       if (name == undefined || name == 'index' || name == 'login') {
  //         next()
  //       } else {
  //         next();
  //         // if (SessionStorage.getItem('permissions')[name] == undefined && name != 'error403') {
  //         //   next('/error403')
  //         // } else {
  //         //   if (setting.includes(to.name)) {
  //         //     if (SessionStorage.getItem('role') <= 1) {
  //         //       next()
  //         //     } else {
  //         //       next('/error403')
  //         //     }
  //         //   } else {
  //         //     next()
  //         //   }
  //         // }
  //       }
  //     }
  //   } catch (e) {
  //     // console.log(e)
  //   }
  // })

  return Router
}
