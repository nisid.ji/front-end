import {Base} from "./shares";

export default class extends Base {
  authenticateByGoogle() {
    return this.get(`account/authenticate/google/`);
  }
  logout(){
    return this.post('account/logout/', {});
  }
  validateByToken(token) {
    return this.post(`account/authenticate/google/`, {token});
  }

  read(params = {}) {
    // console.log('read:', this)
  	return this.get(`/api/v1/role`, params).then(this.response)
  }

  add(form) {
  	return this.post(`management/account/`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`management/account/${id}/`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`management/account/${id}/`).then(this.response)
  }

   readpermission(params = {}) {
  	return this.get(`management/account/permission/`, params).then(this.response)
  }

   addPermission(form) {
  	return this.post(`management/account/permission/`, form).then(this.response)
  }

   findPermission(id) {
  	return this.get(`management/account/permission/${id}/`).then(this.response)
  }

}
