import {Base} from "./shares";

export default class extends Base {
  async read(params = {}) {    
  	return this.get(`/api/v1/game`, params).then(this.response)
  }
}