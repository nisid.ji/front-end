import { Notify, LocalStorage, SessionStorage } from 'quasar'
import AccountService from './account'
import ProviderService from './provider'
import MessageService from './message'
import CompanyService from './company'
import TypeGameService from './typegame'
import MemberService from './member'
import StaffService from './staff'
import RoleService from './role'
import ContentType from './contenttype'
import Permission from './permission'
import Subscribe from './subscribe'
import Game from './game'
import ServiceProvider from './serviceprovider'
import Credit from './credit'
import Group from './group'
import Report from './report'

export default {    
    account: new AccountService(),   
    provider: new ProviderService(),
    message: new MessageService(),
    company: new CompanyService(),
    typegame: new TypeGameService(),
    member: new MemberService(),
    staff: new StaffService(),
    role: new RoleService(),
    contenttype: new ContentType(),
    permission: new Permission(),
    subscribe: new Subscribe(),
    game: new Game(),
    serviceprovider: new ServiceProvider(),
    credit: new Credit(),
    group: new Group(),
    report: new Report(),
    
    updateindex (data) {
    	data.forEach((row, index) => {
        	row.No = index + 1
      	})         
      	return data
    },

    success (message) {
      Notify.create({
          message: message,
          type: 'positive',
          position: 'top-right',
          // color: 'primary',
        })
    },

    error (response, router) {
      if(response){
        if (response.status == 500) {
          Notify.create({
            message: 'Server error',
            type: 'negative',
            position: 'top-right'
          })   
        }else if(response.status == 401){
          SessionStorage.clear()
          Notify.create({
            message: response.data.message,
            type: 'negative',
            position: 'top-right'
          })
          router.push({name: 'login'})
  
        }else{        
          Notify.create({
            message: response.data.message,
            type: 'negative',
            position: 'top-right'
          })
        }
      }else{
        Notify.create({
          message: 'Server error',
          type: 'negative',
          position: 'top-right'
        })  
      }
         
    },
}
