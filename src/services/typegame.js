import {Base} from "./shares";

export default class extends Base {
  async read(params = {}) {    
  	return this.get(`/api/v1/game-type`, params).then(this.response)
  }

  async add(form) {
  	return this.post(`api/v1/game-type`, form).then(this.response)
  }

  async update(form, id) {
  	return this.put(`api/v1/game-type/${id}`, form).then(this.response)
  }

  async remove(id) {
  	return this.delete(`api/v1/game-type/${id}`).then(this.response)
  }   

  async readtypegame() {
    return this.get(`api/v1/typegame`).then(this.response)
  }

  // async typegamecompany(form) {
  //   return this.get(`api/v1/typegame`, form).then(this.response)
  // }
}
