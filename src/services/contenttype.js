import {Base} from "./shares";

export default class extends Base {
  async read(params = {}) {        
  	return this.get(`/api/v1/content-type`, params).then(this.response)
  }


}
