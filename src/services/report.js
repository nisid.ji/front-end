import {Base} from "./shares";

export default class extends Base {
  async read(params = {}) {       
  	return this.get(`/api/report/dashboard`, params).then(this.response)
  } 
}