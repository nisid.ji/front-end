import {Base} from "./shares";

export default class extends Base {
  async deposit (form) {
    return this.post(`/api/v1/credit/deposit`, form).then(this.response)
  }

  async withdraw (form) {
    return this.post(`/api/v1/credit/withdraw`, form).then(this.response)
  } 
}
