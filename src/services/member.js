import { Base } from "./shares";

export default class extends Base {
  async read(params = {}) {
    return this.get(`/api/v1/member`, params).then(this.response);
  }

  async add(form) {
    return this.post(`api/v1/member`, form).then(this.response);
  }

  async update(form, id) {
    return this.put(`api/v1/member/${id}`, form).then(this.response);
  }

  async remove(id) {
    return this.delete(`api/v1/member/${id}`).then(this.response);
  }

  async readByCompany(form) {
    return this.get(`/api/v1/memberbycompany`, form).then(this.response);
  }

  async checkcredit(company) {
    return this.get(`/api/v1/checkcredit`, company).then(this.response);
  }

  async readdownline(params = {}) {
    return this.get(`/api/v1/memberbyowner`, params).then(this.response);
  }

  async authenticate(form) {
    return this.post(`/api/v1/authenticate/login`, form).then(this.response);
  }

  async resetpassword(form) {
    return this.post(`/api/v1/authenticate/reset-password`, form).then(
      this.response
    );
  }

  async resetpasswordmember(form, id) {
    return this.post(`/api/v1/member/${id}/reset-password`, form).then(
      this.response
    );
  }

  async logout() {
    return this.post(`/api/v1/authenticate/logout`).then(this.response);
  }
}
