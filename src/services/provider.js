import {Base} from "./shares";

export default class extends Base {
  read(params = {}) {    
  	return this.get(`/api/v1/provider`, params).then(this.response)
  }

  add(form) {
  	return this.post(`api/v1/provider`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`api/v1/provider/${id}`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`api/v1/provider/${id}`).then(this.response)
  }   

  find_by_id(id) {
    return this.get(`api/v1/provider/${id}`).then(this.response)
  }

  find_game_by_provider(id) {
    return this.get(`api/v1/provider/${id}/game`).then(this.response)
  }

  update_game(form, id) {
    return this.put(`api/v1/game/${id}`, form).then(this.response)
  }


}
