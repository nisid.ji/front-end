import {Base} from "./shares";

export default class extends Base {
  async read(id) {       
  	return this.get(`/api/v1/company/${id}/subscribe`).then(this.response)
  }

  async add(form) {
  	return this.post(`api/v1/staff`, form).then(this.response)
  }

  async update(form, id) {
  	return this.put(`api/v1/staff/${id}`, form).then(this.response)
  }

  async remove(id) {
  	return this.delete(`api/v1/staff/${id}`).then(this.response)
  }   
}
