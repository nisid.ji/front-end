import axios from "axios";


const getUri = function () {
  // process.env.NODE_ENV = 'staging'
  if (process.env.NODE_ENV === 'development') {
    // return 'https://rms-dev-api.stm.trueid.net/api/'
    return 'http://127.0.0.1:5000/'
  } else if (process.env.NODE_ENV === 'staging') {
    return 'https://rms-stg-api.stm.trueid.net/api/'
  } else if (process.env.NODE_ENV === 'local') {
    return 'http://127.0.0.1:8000/api/'
  } else if (process.env.NODE_ENV === 'pre-production') {
    return 'http://10.18.12.194/api/'
  } else if (process.env.NODE_ENV === 'production') {
    return 'https://rmsapi.stm.trueid.net/api/'
  } else {
    return 'https://rms-stg-api.stm.trueid.net/api/'
  }
};

export class Token {
  constructor() {
    let config = {
      baseURL: getUri(),
      timeout: 60 * 1000, // Timeout
      xsrfCookieName: 'XSRF-TOKEN', // default
      xsrfHeaderName: 'X-XSRF-TOKEN', // default
      'Access-Control-Allow-Origin': '*',
      withCredentials: true, // Check cross-site Access-Control,
    };
    this.http = axios.create(config);
    // axios.defaults.headers.common['Token'] = '84975fb1-b43b-441a-9a26-7f39f3eceb87';
    this.setToken();
  }

  setToken() {
    let TOKEN = localStorage.getItem('token');
    this.http.defaults.headers.common['Authorization'] = TOKEN;
  }

  async delete(url) {
    this.setToken()
    return this.http.delete(url)
  }

  async get(url, params = {}) {
    this.setToken();

    return this.http.get(url, {params})
  }

  async post(url, data) {
    this.setToken()
    return this.http.post(url, data)
  }

  async put(url, data) {
    this.setToken()
    return this.http.put(url, data)
  }

  async patch(url, data) {
    this.setToken()
    return this.http.patch(url, data)
  }
}
