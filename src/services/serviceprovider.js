import {Base} from "./shares";

export default class extends Base {
  read(provider) {    
  	return this.get(`/api/v1/provider/${provider}/service`).then(this.response)
  }

  add(form, provider) {
  	return this.post(`api/v1/provider/${provider}/service`, form).then(this.response)
  }

  update(form, provider, id) {
  	return this.put(`api/v1/provider/${provider}/service/${id}`, form).then(this.response)
  }

  remove(id, provider) {
  	return this.delete(`api/v1/provider/${provider}/service/${id}`).then(this.response)
  }   
}
