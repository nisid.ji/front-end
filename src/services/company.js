import {Base} from "./shares";

export default class extends Base {
  read(params = {}) {    
  	return this.get(`/api/v1/company-full`, params).then(this.response)
  }

  add(form) {
  	return this.post(`api/v1/company-full`, form).then(this.response)
  }

  update(form, id) {
  	return this.put(`api/v1/company-full/${id}`, form).then(this.response)
  }

  status(form, id) {
    return this.put(`api/v1/company/${id}`, form).then(this.response)
  }

  remove(id) {
  	return this.delete(`api/v1/company-full/${id}`).then(this.response)
  }

  find_by_id(id) {
    return this.get(`api/v1/company/${id}`).then(this.response)
  }

  companydetail(id) {
    return this.post(`api/v1/companydetail`, id).then(this.response)
  }
}
