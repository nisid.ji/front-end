import axios from "axios"
import { LocalStorage, SessionStorage } from 'quasar'
import {api} from "boot/axios";
// import '../plugins/axios'
// const getUri = function () {
//   return 'http://localhost:5000/'
// };

export class Base {
  constructor() {
    this.page = -1;
    let config = {
      // baseURL: 'https://api.smart-exchange.io',
      timeout: 60 * 1000, // Timeout
      xsrfCookieName: "XSRF-TOKEN", // default
      xsrfHeaderName: "X-XSRF-TOKEN", // default
      withCredentials: true // Check cross-site Access-Control,
    };

    this.http = axios.create(config);
    this.setToken();
  }

  setToken() {
    this.http.interceptors.request.use(function (config) {
      const token =  SessionStorage.getItem('key');
      if (token) {
        config.headers.Authorization = token;
      }
      return config;
    });
    // const TOKEN = SessionStorage.getItem('key');
    // // const TOKEN = "uOJn1AUq6HRsDTgGQAidkzE5ZiZ1AswGYA+esz0bRNTobE0PgIWfk566JyIEKFI7CkFHU/xY7WLj+iyksFDfZw==kldkldodododmIUTTjc";
    // if (TOKEN) {
    //   // this.http.defaults.headers.Authorization = `bearer ${TOKEN}`;
    //   this.http.defaults.headers.common['Authorization'] = TOKEN;
    // } else {
    //   delete this.http.defaults.headers.common["Authorization"];
    // }
  }

  response(res) {
    let { data } = res;
    if (data) {
      return Promise.resolve(data);
    }
    return Promise.reject(res.response);
  }

  handleError = error => {
    return Promise.reject(error);
  };

  responseResultOnPage({ data }) {
    let { page, results } = data;
    this.page = page;
    return results;
  }

  delete(url) {
    this.setToken()
    return this.http.delete(url)
  }

  get(url, params = {}) {
    this.setToken();
    return this.http.get(url, { params })
  }

  post(url, data) {
    this.setToken()
    return this.http.post(url, data)
  }

  put(url, data) {
    this.setToken()
    return this.http.put(url, data)
  }

  patch(url, data) {
    this.setToken()
    return this.http.patch(url, data)
  }
}
