import {Base} from "./shares";

export default class extends Base {
  async read(params = {}) {       
  	return this.get(`/api/v1/role`, params).then(this.response)
  }

  async add(form) {
  	return this.post(`api/v1/role`, form).then(this.response)
  }

  async update(form, id) {
  	return this.put(`api/v1/role/${id}`, form).then(this.response)
  }

  async remove(id) {
  	return this.delete(`api/v1/role/${id}`).then(this.response)
  }   
}
